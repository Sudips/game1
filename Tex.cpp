#include<sdl/sdl.h>
#include<sdl/sdl_opengl.h>

GLuint loadTexture(const char* filename)
{
    SDL_Surface* img=SDL_LoadBMP(filename);
    SDL_Surface* optimized=SDL_DisplayFormat(img);
    SDL_FreeSurface(img);
    GLuint id;
    glGenTextures(1,&id);
    glBindTexture(GL_TEXTURE_2D,id);
    gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGBA32F_ARB,img->w,img->h,GL_BGR_EXT,GL_UNSIGNED_BYTE,img->pixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    SDL_FreeSurface(optimized);
    return id;
}
void ImgHolder(int W,int H)
{
    glBegin(GL_QUADS);
    glColor3f(1.0,1.0,1.0);
    glTexCoord2f(0.0,0.0);
    glVertex2f(W/4,0);
    glTexCoord2f(0.0,1.0);
    glVertex2f(0,H);
    glTexCoord2f(1.0,1.0);
    glVertex2f(W,H);
    glTexCoord2f(1.0,0.0);
    glVertex2f(W-W/4,0);
    glEnd();
}
