#ifndef CAR_H_INCLUDED
#define CAR_H_INCLUDED

class PlayerCar{
                 float carX,carY,carW,carH,LR;
                 public:
                 PlayerCar(); //constructor
                 PlayerCar(float posX,float posY,float posW,float posH,float lr);
                 void buildCar();
                 void moveCar(bool,bool); // LR movement
               };

#endif // CAR_H_INCLUDED
