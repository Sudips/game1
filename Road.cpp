#include<gl/gl.h>
#include "road.h"

CustomRoad::CustomRoad(float x,float y,float w,float h)
{
    StripeX=x;
    StripeY=y;
    StripeW=w;
    StripeH=h;
    IFactor=0;
}
void CustomRoad::DrawStripe()
{
    glBegin(GL_QUADS);
    //for(StripeY=0;StripeY<560;StripeY+=100)

       glColor3f(0.0,0.0,0.0);
       glVertex2f(StripeX,StripeY);
       glVertex2f(StripeX,StripeY+StripeH+IFactor);
       glVertex2f(StripeX+StripeW+IFactor,StripeY+StripeH+IFactor);
       glVertex2f(StripeX+StripeW+IFactor,StripeY);

    glEnd();
}
void CustomRoad::Speed(float Vel)
{
    StripeY+=2*Vel;
    StripeH+=Vel/10;
    StripeW+=Vel/40;
    if(StripeY>560)
    {
        StripeY=0;
        StripeH=30;
        StripeW=15;
    }
}

