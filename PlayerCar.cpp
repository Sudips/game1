#include<sdl/sdl_opengl.h>
#include"car.h"

PlayerCar::PlayerCar(float posX,float posY,float posW,float posH,float lr)
{
    carX=posX;
    carY=posY;
    carW=posW;
    carH=posH;
    LR=lr;
}

void PlayerCar::moveCar(bool left,bool right)
{
    if(left&&carX>0)
    {
        carX-=LR;
    }
    if(right&&carX<720-carW)
    {
        carX+=LR;
    }
}

void PlayerCar::buildCar()
{
    glBegin(GL_QUADS);
    glColor3f(0.0,1.0,0.0);
    glVertex2f(carX,carY);
    glVertex2f(carX,carY+carH);
    glVertex2f(carX+carW,carY+carH);
    glVertex2f(carX+carW,carY);
    glEnd();
}
