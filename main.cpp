#include<sdl/sdl.h>
#include<sdl_opengl.h>
#include<gl/glu.h>
#include "car.h"
#include "Tex.h"
#include "road.h"
const float W=720;
const float H=560;
GLuint tex;
void init()
{
    glClearColor(0.25,0.25,0.25,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,W,H,0);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    tex=loadTexture("road.bmp");
}

    void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glBindTexture(GL_TEXTURE_2D,tex);
    ImgHolder(W,H);
}

int main(int argc,char* argv[])
{
    SDL_WM_SetCaption("Game Window",NULL);
    SDL_SetVideoMode(W,H,32,SDL_OPENGL|SDL_SWSURFACE);
    glViewport(0,0,W,H);

    bool isRunning=true;
    bool left=false,right=false,accelerate=false;
    float speed=50.0;
    SDL_Event event;

    init();
    PlayerCar car(200.0,400.0,50.0,100.0,0.5);
    CustomRoad Road(350.0,0.0,15.0,30.0);
    while(isRunning)
    {
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT||event.key.keysym.sym==SDLK_ESCAPE)
            isRunning=false;
            if(event.type==SDL_KEYDOWN)
            {
               if(event.key.keysym.sym==SDLK_RIGHT)
               right=true;
               if(event.key.keysym.sym==SDLK_LEFT)
               left=true;
               if(event.key.keysym.sym==SDLK_UP)
               accelerate=true;
            }
            if(event.type==SDL_KEYUP)
            {
                if(event.key.keysym.sym==SDLK_RIGHT)
                right=false;
                if(event.key.keysym.sym==SDLK_LEFT)
                left=false;
                if(event.key.keysym.sym==SDLK_UP)
                accelerate=false;

            }

        }

        display();
        car.buildCar();
        car.moveCar(left,right);
        Road.DrawStripe();
        if(accelerate==true)
        {
            Road.Speed(speed);
            if(speed>1000.0)
            {
                speed=1000.0;
            }
        }
        SDL_GL_SwapBuffers();
    }

    SDL_Quit();
    return 0;
}
